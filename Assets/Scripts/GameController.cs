﻿using System;
using UnityEngine;
using System.Diagnostics;
using Assets.Scripts;
using Assets.Scripts._base;
using UnityEngine.UI;

public class GameController : UnityBaseBehaviour
{
    public static GameController Instance { get { return GameObject.Find("_Scripts").GetComponent<GameController>(); } }
    
    public Minefield Minefield { get { return GameObject.Find("_Minefield").GetComponent<Minefield>(); } }
    public GameState GameState;

    private Stopwatch Stopwatch = new Stopwatch();    
    private Difficulty difficulty = Difficulty.Easy;
    
    public int MinesLeft
    {
        get { return int.Parse(GameObject.Find("MinesText").GetComponent<Text>().text); }
        set { GameObject.Find("MinesText").GetComponent<Text>().text = value.ToString(); }
    }

    public override void Start()
    {                 
        Stopwatch = new Stopwatch();
        StartNewGame();
    }

    public override void Update()
    {
        if (Stopwatch.IsRunning)
            GameObject.Find("TimeText").GetComponent<Text>().text = Stopwatch.Elapsed.Seconds.ToString();
    }

    public void StartNewGame(string difficultyName = "")
    {        
        if(!string.IsNullOrEmpty(difficultyName))
            difficulty = Difficulty.Find(difficultyName);
        MinesLeft = difficulty.MineCount;
        Minefield.CreateTileField(difficulty);
        Stopwatch.Reset();
        GameObject.Find("TimeText").GetComponent<Text>().text = "0";
        ShowCurrentHighscore();        
        SetGameState(GameState.Neutral);
        AdjusPosition();
    }

    public void SetGameState(GameState state)
    {
        GameState = state;
        Stopwatch.Stop();
        if (state == GameState.Win)
            UpdateHighscore();
        GameObject.Find("PlayButton").GetComponent<Button>().image.sprite = state.ToSprite();
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResetHighscore()
    {
        PlayerPrefs.DeleteAll();
        ShowCurrentHighscore();
    }

    public void StartTimer()
    {
        Stopwatch.Start();
    }

    private void ShowCurrentHighscore()
    {
        GameObject.Find("HighscoreText").GetComponent<Text>().text = PlayerPrefs.HasKey(difficulty.Key) ? PlayerPrefs.GetInt(difficulty.Key).ToString() : "-";
    }

    private void AdjusPosition()
    {
        var topCanvas = GameObject.Find("TopCanvas");
        var bottomCanvas = GameObject.Find("BottomCanvas");
        Minefield.transform.position = new Vector2(-(Minefield.xTotal - 1f) / 2f, -(Minefield.yTotal - 1f) / 2f);
        topCanvas.transform.position = new Vector2(0, (Minefield.yTotal - 1f) / 2f + 2f);
        bottomCanvas.transform.position = new Vector2(0, -(Minefield.yTotal - 1f) / 2f - 3f);
        RectTransform top = (RectTransform)topCanvas.transform;
        top.sizeDelta = new Vector2(Minefield.xTotal + 3, 3);

        RectTransform bottom = (RectTransform)bottomCanvas.transform;
        bottom.sizeDelta = new Vector2(Minefield.xTotal + 3, 3);
        Camera.main.orthographicSize = Math.Min(difficulty.Height - 2, difficulty.Width - 2);
    }

    private void UpdateHighscore()
    {
        int time = Stopwatch.Elapsed.Seconds;
        if (!PlayerPrefs.HasKey(difficulty.Key) || PlayerPrefs.GetInt(difficulty.Key) > time)
        {
            PlayerPrefs.SetInt(difficulty.Key, time);
            ShowCurrentHighscore();
        }
    }

}
