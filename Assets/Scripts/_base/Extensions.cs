﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts._base
{
    public static class Extensions
    {
        public static void Apply<T>(this IEnumerable<T> collection, Action<T> action )
        {
            foreach (T item in collection)
                action(item);
        }
    }
}