﻿using System;
using System.Linq;

namespace Assets.Scripts
{
    public class Difficulty
    {
        public int Width;
        public int Height;
        public int MineCount;
        public string Key
        {
            get { return string.Format("{0}-{1}-{2}", Width, Height, MineCount); }
        }

        private Difficulty(int width, int height, int mineCount)
        {
            Width = width;
            Height = height;
            MineCount = mineCount;
        }
        
        public static Difficulty Easy = new Difficulty(13, 13, 12);
        public static Difficulty Medium = new Difficulty(15, 15, 20);
        public static Difficulty Hard = new Difficulty(40, 25, 90);

        public static Difficulty Find(string name)
        {
            var fieldInfo = typeof(Difficulty).GetFields().FirstOrDefault(info => info.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            return fieldInfo != null ? (Difficulty)fieldInfo.GetValue(null) : Easy;
        }
    }
}