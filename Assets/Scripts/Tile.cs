﻿using System;
using Assets.Scripts;
using UnityEngine;
using Assets.Scripts._base;

public class Tile : UnityBaseBehaviour
{
    
    public bool IsMine;
    public Sprite[] EmptyFieldSprites;
    public int X;
    public int Y;
    public bool IsRevealed;
    public FieldState State { get; private set; }

    public event EventHandler<EventArgs> Click;


    public static Tile CreateNewTile(int x, int y, Transform transform)
    {                      
        GameObject tileObject = (GameObject)Instantiate(Resources.Load("Prefabs/Tile"));
        tileObject.transform.parent = transform;        
        Tile script = tileObject.GetComponent<Tile>();        
        script.transform.position = new Vector2(script.X = x, script.Y = y);
        return script;
    }

    private Sprite currentSprite
    {
        get { return GetComponent<SpriteRenderer>().sprite; }
        set { GetComponent<SpriteRenderer>().sprite = value;}
    }

    public void SetState(FieldState state)
    {
        State = state;
        GetComponent<BoxCollider2D>().enabled = state == FieldState.Default || state == FieldState.Flag || state == FieldState.Unknown;
        currentSprite = state.ToSprite();
    }


    public void ChangeSpriteToEmpty(int i)
    {
        currentSprite = EmptyFieldSprites[i];
        IsRevealed = true;
    }


    public void ToggleRescudedSprite()
    {
        if (State == FieldState.Default)
        {
            SetState(FieldState.Flag);
            GameController.Instance.MinesLeft --; 
        }
        else if (State == FieldState.Flag)
        {
            SetState(FieldState.Unknown);
            GameController.Instance.MinesLeft++; 
        }
        else
        {
            SetState(FieldState.Default);            
        }
    }
    

    public override void OnMouseUpAsButton()
    {
        if (State != FieldState.Flag)
            RaiseOnClick();
    }

    public override void OnMouseOver()
    {
        if (State == FieldState.Default && !IsRevealed)
            currentSprite = Resources.Load<Sprite>("Pictures/Default_Hover");
        if (Input.GetMouseButtonDown(1) && !IsRevealed)
        {
            ToggleRescudedSprite();
        }
    }

    public override void OnMouseExit()
    {
        if (State == FieldState.Default && !IsRevealed)
            currentSprite = Resources.Load<Sprite>("Pictures/Default");
    }


    protected virtual void RaiseOnClick()
    {
        var handler = Click;
        if (handler != null) handler(this, EventArgs.Empty);
    }
}
