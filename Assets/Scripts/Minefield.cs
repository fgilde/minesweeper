﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts._base;

public class Minefield : UnityBaseBehaviour
{
    bool isFirstClick;
    Tile[,] tiles;

    public int xTotal;
    public int yTotal;
    
    public void CreateTileField(Difficulty difficulty)
    {
        transform.position = new Vector2(0, 0);
        GameObject.FindGameObjectsWithTag("Tile").Apply(Destroy);

        isFirstClick = true;        
        xTotal = difficulty.Width;
        yTotal = difficulty.Height;

        tiles = new Tile[xTotal, yTotal];

        for (var i = 0; i < xTotal; i++)
        {
            for (var j = 0; j < yTotal; j++)
            {
                tiles[i, j] = Tile.CreateNewTile(i, j, transform);
                tiles[i, j].Click += (o, a) => OnClickTile((Tile)o);
            }
        }
    }

    public void OnClickTile(Tile tile)
    {
        if (isFirstClick)
            CreateMines(tile);

        if (tile.IsMine)
            LoseGame(tile);
        else
        {
            Reveal(tile.X, tile.Y, new bool[xTotal, yTotal]);
            if (IsGameWon())
                WinGame();
        }
    }

    private void CreateMines(Tile tileToIgnore)
    {
        isFirstClick = false;
        GameController.Instance.StartTimer();

        int minesLeft = GameController.Instance.MinesLeft;
        int fieldCounter = xTotal * yTotal;

        foreach (var tile in tiles.Cast<Tile>().Where(tile => tile != tileToIgnore))
        {
            if (Random.value <= (float)minesLeft / fieldCounter)
            {
                tile.IsMine = true;
                minesLeft--;
            }
            fieldCounter--;
        }
    }

    public void Reveal(int x, int y, bool[,] isRevealed)
    {
        if (!isRevealed[x, y])
        {
            isRevealed[x, y] = true;
            int neighbours = CountNeighbours(x, y);
            tiles[x, y].ChangeSpriteToEmpty(neighbours);
            if (neighbours == 0)
                NeighbourPositions(x, y).Apply(n => Reveal(n[0], n[1], isRevealed) );              
        }
    }

    public bool IsGameWon()
    {
        return tiles.Cast<Tile>().All(tile => tile.IsRevealed || tile.IsMine);
    }

    public void WinGame()
    {        
        tiles.Cast<Tile>().Where(tile => tile.IsMine).Apply(tile => tile.SetState(FieldState.RescuedBomb));
        GameController.Instance.SetGameState(GameState.Win);
    }

    public void LoseGame(Tile bomb)
    {
        foreach (var tile in tiles)
        {
            tile.GetComponent<BoxCollider2D>().enabled = false;
            if (tile.IsMine)
                tile.SetState(FieldState.Bomb);
        }
        bomb.SetState(FieldState.DeadlyBomb);
        GameController.Instance.SetGameState(GameState.Lose);                
    }
   

    private int CountNeighbours(int x, int y)
    {
        return NeighbourPositions(x, y).Select(ints => tiles[ints[0], ints[1]]).Count(tile => tile.IsMine);
    }

    private IEnumerable<int[]> NeighbourPositions(int x, int y)
    {
        return new[]
        {
            new[] {x, y - 1},
            new[] {x, y + 1},
            new[] {x + 1, y - 1},
            new[] {x + 1, y},
            new[] {x + 1, y + 1},
            new[] {x - 1, y - 1},
            new[] {x - 1, y},
            new[] {x - 1, y + 1}
        }.Where(ints => ints[0] >= 0 && ints[1] >= 0 && ints[0] < xTotal && ints[1] < yTotal);
    }
}
