﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public enum FieldState
    {
        Default,
        Bomb,
        DeadlyBomb,
        Flag,
        RescuedBomb,
        Unknown
    }

    public enum GameState
    {
        Neutral,
        Win,
        Lose
    }
    
    public static class StateHelper
    {
        private static readonly Dictionary<string, Sprite> cache = new Dictionary<string, Sprite>();
        public static Sprite ToSprite(this FieldState state)
        {
            return GetCached(state.ToString());
        }

        public static Sprite ToSprite(this GameState state)
        {
            return GetCached(state.ToString());
        }

        private static Sprite GetCached(string key)
        {            
            if (cache.ContainsKey(key))
                return cache[key];

            var sprite = Resources.Load<Sprite>("Pictures/" + key);
            cache.Add(key, sprite);
            return sprite;
        }
    }

}